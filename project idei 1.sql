USE EBAIE;

create table Produse (
	cod_Produs varchar(25) PRIMARY KEY,
	denumire varchar(50),
	ID_category int, FOREIGN KEY (ID_category) REFERENCES Categorii(ID_category),
	IDB int, FOREIGN KEY (IDB) REFERENCES Branduri(IDB),
	material varchar(25),
	dimensiune varchar(25),
	stoc int, 
	UM varchar(5),
	pret_vechi_lei decimal(10, 2),
	pret_nou_lei decimal(10, 2)
);

alter table Produse drop column pret_nou_lei;

Create table Categorii(
	ID_category int PRIMARY KEY,
	ID_parent int references Categorii(ID_category),
	categorie varchar(50)
);

SELECT * FROM Categorii;

Create view Categorii_Subcategorii
	AS
		SELECT c2.categorie as Nume_categorie, c1.categorie as Nume_subcategorie 
		FROM Categorii c1 JOIN Categorii c2 on 
		c2.ID_category = c1.ID_parent 
	GO

create table Branduri(
	IDB int PRIMARY KEY,
	Brand varchar(20),
	Tara varchar(20)
);

drop table Comenzi;

Create table Produse_Comenzi (
	Nr_Comanda numeric(10,0),
	FOREIGN KEY (Nr_Comanda) REFERENCES Comenzi(Nr_Comanda),
	cod_Produs varchar(25),
	FOREIGN KEY (cod_Produs) REFERENCES Produse(cod_Produs),
	ID_Cos int,
	FOREIGN KEY (ID_Cos) REFERENCES Cosul_Meu(ID_Cos)
);

create table Autentificare(
	cod_client INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Nume varchar(50),
	Prenume varchar(50),
	Email varchar(50) UNIQUE,
	parola varchar(50)
);

select * from Autentificare;

--DROP TABLE Autentificare;
--DROP TABLE Cosul_Meu;

SET IDENTITY_INSERT dbo.Autentificare ON;
	insert into Autentificare VALUES('Dansatoarea', 'Floricica', 'floricica_dansatoarea@gmail.com', 'savafierusine');
	insert into Autentificare(Nume, Prenume, Email) VALUES('Diaconescu', 'Dan', 'danDi@gmail.com');
	insert into Autentificare(Nume, Prenume, Email) VALUES('Vadim', 'Tudor', 'vctudor@gmail.com');
SET IDENTITY_INSERT Autentificare OFF;

ALTER table Autentificare ADD CONSTRAINT def_passw default '12345' for parola;

drop table Cosul_Meu;

create table Cosul_Meu(
	ID_Cos int PRIMARY KEY
);

Create table Comenzi (
	Nr_Comanda numeric(10,0) PRIMARY KEY,
	ID_Cos int,
	FOREIGN KEY (ID_Cos) REFERENCES Cosul_Meu(ID_Cos),
	Starea_comenzii varchar(25),
	cod_client INT,
	FOREIGN KEY (cod_client) REFERENCES Autentificare(cod_client)
);

CREATE TABLE Produse_Comenzi(
	cod_Produs varchar(25),	
	FOREIGN KEY (cod_Produs) REFERENCES Produse(cod_Produs),
	ID_Cos int,
	FOREIGN KEY (ID_Cos) REFERENCES Cosul_Meu(ID_Cos),
	cantitate int
);

create table Oferte(
	cod_Produs varchar(25),	
	FOREIGN KEY (cod_Produs) REFERENCES Produse(cod_Produs),
	pret_nou_lei decimal(10, 2)
);

EXEC sp_rename 'Produse_Comenzi' , 'Produse_Cos';

ALTER VIEW Subtotal_Comenzi
	AS
		SELECT	c.ID_Cos as Numar_Cos, p.denumire as Produs, pc.cantitate, 
				(p.pret_vechi_lei* pc.cantitate) as Subtotal
		FROM Cosul_Meu c, Produse p JOIN Produse_Cos pc on 
		p.cod_Produs = pc.cod_Produs 
		WHERE c.ID_Cos = pc.ID_Cos
	GO

SELECT * FROM Subtotal_Comenzi ORDER BY Numar_Cos;

Create view Total_Comanda
	AS
		SELECT c.ID_Cos as Numar_Cos, 
				SUM(p.pret_vechi_lei* pc.cantitate) as Total 
		FROM Cosul_Meu c
		JOIN Produse_Cos pc on pc.ID_Cos = c.ID_Cos
		JOIN Produse p on pc.cod_Produs = p.cod_Produs
		WHERE c.ID_Cos = c.ID_Cos
		GROUP BY c.ID_Cos
	GO

Create view Total_Comanda
	AS
		SELECT c.ID_Cos as Numar_Cos, 
				SUM(p.pret_vechi_lei* pc.cantitate) as Total 
		FROM Cosul_Meu c
		JOIN Produse_Cos pc on pc.ID_Cos = c.ID_Cos
		JOIN Produse p on pc.cod_Produs = p.cod_Produs
		WHERE c.ID_Cos = c.ID_Cos
		GROUP BY c.ID_Cos
	GO

SELECT * FROM Produse;
SELECT * FROM Produse_Cos;

DECLARE DefaultCursor CURSOR GLOBAL
FOR 
SELECT c.ID_Cos as Numar_Cos, 
				SUM(p.pret_vechi_lei* pc.cantitate) as Total 
		FROM Cosul_Meu c
		JOIN Produse_Cos pc on pc.ID_Cos = c.ID_Cos
		JOIN Produse p on pc.cod_Produs = p.cod_Produs
		WHERE c.ID_Cos = c.ID_Cos
		GROUP BY c.ID_Cos

OPEN DefaultCursor;
FETCH NEXT FROM DefaultCursor;

CLOSE DefaultCursor;
DEALLOCATE DefaultCursor;

--Proceduri Stocate
--Create/Insert

SELECT * FROM Produse;

CREATE PROCEDURE Create_Product 
	@cod_Produs nvarchar(25), 
	@denumire nvarchar(50), 
	@ID_category int, 
	@IDB int, 
	@material nvarchar(25), 
	@dimensiune nvarchar(25), 
	@stoc int, 
	@UM nvarchar(5), 
	@pret_vechi_lei decimal(10,2)
AS
INSERT INTO Produse VALUES(@cod_Produs, @denumire, @ID_category, @IDB, @material, @dimensiune, @stoc, @UM, @pret_vechi_lei);

CREATE PROCEDURE Create_Category
	@ID_category int,
	@ID_parent int,
	@categorie nvarchar(50)
AS
INSERT INTO Categorii VALUES(@ID_category, @ID_parent, @categorie);

CREATE PROCEDURE Create_User
	@Nume nvarchar(50),
	@Prenume nvarchar(50),
	@Email nvarchar(50),
	@parola nvarchar(50)
AS 
INSERT INTO Autentificare VALUES( @Nume, @Prenume, @Email, @parola);

CREATE PROCEDURE Create_Brand
	@IDB int,
	@Brand nvarchar(20),
	@Tara nvarchar(20)
AS 
INSERT INTO Branduri VALUES(@IDB, @Brand, @Tara);

CREATE PROCEDURE Create_Offer
	@cod_Produs nvarchar(25),	
	@pret_nou_lei decimal(10, 2)
AS 
INSERT INTO Oferte VALUES(@cod_Produs, @pret_nou_lei);

--Read/View

CREATE PROCEDURE dbo.View_Product_byName @denumire nvarchar(50)
AS 
	SELECT * FROM Produse WHERE denumire LIKE @denumire + '%';

CREATE PROCEDURE dbo.View_Product_byID @cod_Produs nvarchar(25)
AS 
	SELECT * FROM Produse WHERE cod_Produs = @cod_Produs;

CREATE PROCEDURE ViewAllProducts
AS
	SELECT * FROM Produse
GO;

CREATE PROCEDURE ViewAllBrands
AS
	SELECT * FROM Branduri
GO;

CREATE PROCEDURE ViewAllOffers
AS
	SELECT * FROM Oferte
GO;

CREATE PROCEDURE ViewAllOrders
AS
	SELECT * FROM Comenzi
GO;

CREATE PROCEDURE ViewAllCategories
AS
	SELECT * FROM Categorii
GO;

--Update

CREATE PROCEDURE Update_Price @cod_Produs nvarchar(25), @pret_vechi_lei decimal(10,2)
AS 
UPDATE Produse SET pret_vechi_lei = @pret_vechi_lei WHERE cod_Produs = @cod_Produs;

CREATE PROCEDURE Update_Password @parola nvarchar(50), @Email nvarchar(50)
AS 
UPDATE Autentificare SET parola = @parola WHERE Email = @Email;

CREATE PROCEDURE Update_Offer @cod_Produs nvarchar(25), @pret_nou_lei decimal(10, 2)
AS 
UPDATE Oferte SET pret_nou_lei = @pret_nou_lei WHERE cod_Produs = @cod_Produs;


--Delete

CREATE PROCEDURE Delete_Product @cod_Produs nvarchar(25)
AS 
	DELETE FROM Produse WHERE cod_Produs = @cod_Produs;

CREATE PROCEDURE Delete_Brand_byID @IDB int
AS 
	DELETE FROM Branduri WHERE IDB = @IDB;

CREATE PROCEDURE Delete_Brand_byName @Brand nvarchar(20)
AS 
	DELETE FROM Branduri WHERE Brand = @Brand;

CREATE PROCEDURE Delete_User @Nume nvarchar(50)
AS 
	DELETE FROM Autentificare WHERE Nume = @Nume;

CREATE PROCEDURE Delete_Offer @cod_Produs nvarchar(25)
AS 
	DELETE FROM Oferte WHERE cod_Produs = @cod_Produs;


CREATE PROCEDURE Delete_Order @Nr_Comanda numeric(10,0)
AS 
	DELETE FROM Comenzi WHERE Nr_Comanda = @Nr_Comanda;

--Triggers
--DDL

CREATE TRIGGER safety_check   
ON DATABASE   
FOR DROP_TABLE, ALTER_TABLE   
AS   
   PRINT 'You must disable Trigger "safety" to drop or alter tables!'   
   ROLLBACK;  

--DML
CREATE TRIGGER trDeleteUser
ON Autentificare
FOR DELETE
AS
BEGIN
  PRINT 'YOU CANNOT PERFORM DELETE OPERATION'
  ROLLBACK TRANSACTION
END

--DROP TRIGGER trModifyUser

CREATE TRIGGER trModifyUser
ON Autentificare
FOR UPDATE
AS
BEGIN
  PRINT 'YOU CANNOT PERFORM UPDATE OPERATION WITHOUT PERMISSION'
  ROLLBACK TRANSACTION
END

CREATE TRIGGER trInsertinMyCart
ON Cosul_Meu
FOR INSERT
AS
BEGIN
  PRINT 'YOU CANNOT PERFORM INSERT OPERATION'
  ROLLBACK TRANSACTION
END

--Backup/Restore

-- Create the backup_restore_progress_trace extended event session
--CREATE EVENT SESSION [BackupRestoreTrace] ON SERVER 
--ADD EVENT sqlserver.EBAIE
--ADD TARGET package0.event_file(SET filename=N'BackupRestoreTrace')
--WITH (MAX_MEMORY=4096 KB,EVENT_RETENTION_MODE=ALLOW_SINGLE_EVENT_LOSS,MAX_DISPATCH_LATENCY=5 SECONDS,MAX_EVENT_SIZE=0 KB,MEMORY_PARTITION_MODE=NONE,TRACK_CAUSALITY=OFF,STARTUP_STATE=OFF)
--GO

-- Start the event session  
--ALTER EVENT SESSION [BackupRestoreTrace]  
--ON SERVER  
--STATE = start;  
--GO  

-- Stop the event session  
--ALTER EVENT SESSION [BackupRestoreTrace]  
--ON SERVER  
--STATE = stop;  
--GO  