-- database populating (EBAIE)

insert into Categorii values(1, null, 'Lavoare'), (2, null, 'Cazi de baie')
, (3, null, 'Bideuri'), (5, null, 'Rezervoare'), (4, null, 'Vase WC'), 
(6, null, 'Cabine de dus')
, (7, null, 'Cadite de dus'), (8, null, 'Oglinzi'), (9, null, 'Clapete de actionare'),
 (10, null, 'Baterii de baie')
, (11, null, 'Baterii de bucatarie'), (12, null, 'Mobilier de baie'), (13, null, 'Dusuri'), 
(14, null, 'Paravane de cada')
, (15, null, 'Rame de montaj'), (16, null, 'Instalatii de baie'), (17, null, 'Accesorii'),
 (18, null, 'Siguranta baii')
, (19, null, 'Rigole si sifoane'), (20, null, 'Lichidari')
, (21, 1, 'Lavoare suspendate'), (22, 1, 'Lavoare pe blat'), (23, 1, 'Lavoare pe mobilier'), 
(24, 1, 'Lavoare de colt'),
(25, 1, 'Lavoare duble'), (26, 1, 'Lavoare incastrabile'), (27, 1, 'Spalatoare');

insert into Categorii values(103, 18, 'Bare pentru persoane cu handicap'), 
(104, 18, 'Bare de siguranta'), (105, 18, 'Scaune pentru persoane cu handicap'), 
(106, 18, 'Etiuda'), (107, 19, 'Rigole de dus'),
(108, 19, 'Gratare pentru rigole'), (109, 19, 'Sifoane de pardoseala'),
(110, 19, 'Corpuri pentru sifoane'), (111, 20, 'Accesorii'),
(112, 20, 'Baterii si dusuri'), (113, 20, 'Obiecte sanitare'),
(114, 20, 'Cabine de dus'), (115, 20, 'Cadite de dus'),
(116, 20, 'Mobilier baie'), (117, 20, 'Rame si rezervoare'),
(118, 20, 'Rame aerisire')
;

insert into Categorii values
(49, 8, 'Dreptunghiulare'), (50, 8, 'Patrate'), (51, 8, 'Rotunde')
;

insert into Categorii values (52, 10, 'Baterii lavoar')
, (53, 10, 'Baterii bideu'), (54, 10, 'Baterii cada si dus'), (55, 10, 'Baterii dus'),
 (56, 10, 'Corpuri incastrate')
, (57, 10, 'Baterii cu termostat'), (58, 10, 'Baterii pe lavoar'), 
(59, 10, 'Baterii pe perete pentru lavoar'), 
(60, 10, 'Baterii stative de pardoseala')
, (61, 10, 'Baterii inalte de blat'), (62, 10, 'Baterii joase de blat'), 
(63, 10, 'Baterii pe cada'), (64, 10, 'Baterii cada pe perete')
, (65, 10, 'Parte ingropata'), (66, 10, 'Seturi de baterii'), 
(67, 12, 'Masti pentru lavoare'), (68, 12, 'Dulapuri de baie')
, (69, 13, 'Paneluri de dus'), (70, 13, 'Capete fixe de dus')
, (71, 13, 'Pare de dus'), (72, 13, 'Furtunuri dus'), (73, 13, 'Seturi de dus'), 
(74, 14, 'Paravane mobile')
, (75, 14, 'Paravane fixe'), (76, 15, 'pentru lavoare'), (77, 15, 'pentru vase WC')
, (78, 15, 'pentru bideuri'), (79, 15, 'pentru rigole de dus'), (80, 15, 'pentru pisoare')
, (81, 16, 'Rigole de dus'), (82, 16, 'Gratare pnetru rigole'), (83, 16, 'Sifoane de pardoseala')
, (84, 16, 'Corpuri pentru sifoane'), (85, 16, 'Instalatii cada'), (86, 16, 'Sifoane cadita')
, (87, 16, 'Sifoane lavoar'), (88, 16, 'Ventile'), (89, 16, 'Racorduri flexibile')
, (90, 16, 'Tevi de legatura'), (91, 17, 'Cuiere'), (92, 17, 'Cosuri de gunoi')
, (93, 17, 'Dozatoare sapun'), (94, 17, 'Etajere si rafturi baie'), (95, 17, 'Porthartie')
, (96, 17, 'Portpahare'), (97, 17, 'Portperie'), (98, 17, 'Portperiute')
, (99, 17, 'Portprosoape'), (100, 17, 'Portsapun'), (101, 17, 'Recipient pentru dischete')
, (102, 17, 'Demachiante')
;


insert into Branduri values
(101, 'Alcaplast', 'Cehia'),(102, 'Aquaform', 'Polonia'),(103, 'Arthema', 'Romania'),
(104, 'Carlo Frattini', 'Italia'),
(105, 'Cersanit', 'Polonia'),(106, 'Ferro', 'Polonia'),(107, 'Fischer', 'Austria'),
(108, 'Fluminia', 'China'), (109, 'Florida', 'China'),(110, 'Geberit', 'Elvetia'),(111, 'Grohe', 'Germania'),
(112, 'Hansgrohe', 'Germania'), (113, 'Hatria', 'Italia'),(114, 'Huppe', 'Germania'),(115, 'Ideal Standard', 'Belgia'),
(116, 'Kessel', 'Germania'),
(117, 'Kludi', 'Germania'),(118, 'Kolo', 'Polonia'),(119, 'KolpaSan', 'Slovenia'),
(120, 'Laufen', 'Elvetia'),
(121, 'Mediterraneo', 'China'),(122, 'Novaservis', 'Cehia'),(123, 'Plast-Brno', 'Cehia'),
(124, 'Roca', 'Spania'),
(125, 'SanSwiss', 'Elvetia'),(126, 'Villeroy & Boch', 'Germania')
;

insert into Produse VALUES
('S599-0066', 'Picioare Universale pentru Mobilier', 12, 105, 'metal', '3,8 x 12 x 3,8 (cm)', 44, 'buc', 28.00),
('K98-0035', 'Capac vas WC Merida', 4, 105, 'polipropilena', 'universala', 11, 'buc', 59.00),
('K07-015-EX3', 'Vas WC stativ, Roma', 4, 105, 'ceramica sanitara', '46,5 x 36 (cm)', 11, 'buc', 133.00)
;

insert into Produse VALUES
('K07-016', 'Vas WC stativ, Roma cu iesire verticala', 4, 105, 'ceramica sanitara', '49,0 x 35,5 (cm)', 5, 'buc', 138.00),
('K98-0005', 'Capac vas WC Roma', 4, 105, 'duroplast', 'universala', 5, 'buc', 173.00)
;

insert into Produse VALUES
('BVA2', 'Vasto, Baterie monocomanda pentru lavoar', 52, 106, 'inox', 'mica', 60, 'buc', 95.00),
('BBC5', 'Basic, baterie monocomanda cu montaj pe perete', 52, 106, 'alama/crom', '150�20 (mm)', 30, 'buc', 105.00),
('BSM3', 'Smile, baterie lavoar, cu montaj pe perete', 52, 106, 'cartus ceramic, cromat', '170�20 (mm)', 30, 'buc', 114.00),
('593360', 'Evelin, cada dreptunghiulara', 31, 119, 'acril sanitar', '150 x 70 (cm)', 10, 'buc', 743.00),
('HY902FA 1550X730X770', 'Venus, Cada de baie tip insula', 30, 109, 'acril sanitar', '155 x 73 x 77 (cm)', 10, 'buc', 2577.00),
('UBQ194AVE9W1V-01', 'Aveo New Generation, cada free standing', 30, 126, 'quaryl', '190 x 95 (cm)', 2, 'buc', 28405.00),
('100-091112', 'Nigra, cabina semirotunda, sticla satinata', 41, 102, 'sticla securizata', '80 x 80 x 185 (cm)', 5, 'buc', 697.00),
('92001/1.0', 'Titania Iris, Baterie stativa lavoar', 52, 122, 'cartus ceramic, cromat', 'mare', 45, 'buc', 95.00)
;

insert into Cosul_Meu VALUES
(1001), (1002), (1003), (1004), (1005), (1006), (1007), (1008), (1009), (1010), (1011), (1012), (1013), (1014), (1015), (1016), (1017), (1018), (1019), (1020), (1021), (1022), (1023), (1024);
insert into Autentificare(Nume, Prenume, Email) VALUES('Turul', 'Denes', 'turulmadar@fidesz.hu');
insert into Comenzi VALUES
(1100000001, 1002, 'in curs de procesare', 4), (1100000002, 1003, 'in curs de procesare', 2),(1100000003, 1008, 'in curs de livrare', 6), (1100000004, 1012, 'in curs de livrare', 4),
(1100000005, 1022, 'in curs de livrare', 5);

insert into Produse_Cos VALUES
('100-091112', 1022, 1), ('593360', 1010, 1),('HY902FA 1550X730X770', 1001, 1), ('92001/1.0', 1001, 1),('BVA2', 1017, 1);

insert into Produse_Cos VALUES
('K98-0005', 1002, 4), ('S599-0066', 1003, 4),('K98-0035', 1008, 1), ('K07-016', 1003, 4),('K07-015-EX3', 1008, 1);

insert into Produse_Cos VALUES
('K98-0005', 1004, 4), ('S599-0066', 1006, 4), ('K98-0035', 1017, 1),
('K07-016', 1006, 4), ('K07-015-EX3', 1017, 1);

SELECT * FROM Produse_Cos ORDER BY ID_Cos;

insert into Autentificare(Nume, Prenume, Email) VALUES
('Horvath', 'Lajos-Istvan', 'lajiba_ittvan@fidesz.hu'),
('Dancila', 'Victor Viorel', 'victorita@psd.ro'),('Kaye', 'Danny', 'dannyk12@gmail.com'),
('Memelord', 'Ricardoge', 'ricardota@gmail.com'),('Lily', 'Andrews', 'andrew_s@gmail.com'),
('Pink', 'Guy', 'pinkguy_lmao@gmail.com'),('Ceiber', 'Bullie', 'ceiberbullie@gmail.com'),
('Koszos', 'Hippi', 'koszoshippi@vilagbeke.hu'),('Dee', 'Ellis', 'ellisdee420@yahoo.com');

SELECT * FROM Autentificare;